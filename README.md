# cebrusfs's dotfiles

## INSTALL

```sh
sudo apt-get install python-minimal zsh
```

```sh
bash <(curl -s https://bitbucket.org/cebrusfs/dotfiles/raw/master/fetch)
```

### Mac post install

#### Homebrew

Use `setup.sh` script under `homebrew` directory

#### Topcoder config

```sh
DOTFILE_DIR=~/.dotfiles
ln -s "$DOTFILE_DIR/config/topcoder" "$HOME/Topcoder"
ln -s "$DOTFILE_DIR/config/topcoder/contestapplet.conf" "$HOME/contestapplet.conf"
chflags -h hidden "$HOME/contestapplet.conf"

touch "$HOME/contestapplet.conf.bak"
chflags -h hidden "$HOME/contestapplet.conf.bak"
```


## Key binding

* Terminator / iTerm2: Not use split window
* `Ctrl+A` as tmux prefix

### Tmux Windows (tabs)
```
c   create window
w   list windows
n   next window
p   previous window
f   find window
,   name window
&   kill window
```

### Tmux Panes (splits)
```
<arrow>           move between splits (default)
q                 show pane numbers
                  (Show pane numbers, when the numbers show up type the key to goto that pane)

%                 vertical split      (default)
s                 vertical split
"                 horizontal split    (default)
v                 vertical split

x                 kill pane           (default)
k                 kill pane
```


### Vim tabs
TODO

### Vim Split
```
:vsp          vertical split
:sp           horizontal split
C-w <arrow> move between split
```
